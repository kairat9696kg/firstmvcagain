﻿using AutoMapper;
using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.UnitOfWorks;
using FirstMVCAgain.UI.Models;
using FirstMVCAgain.UI.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.UI.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        public AccountService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager, SignInManager<User> signInManager)
        {

            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            if (signInManager == null)
                throw new ArgumentNullException(nameof(signInManager));
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
            _signInManager = signInManager;
        }

        public User GetUser(string email)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = unitOfWork.User.GetUserList(email);
                return user;
            }
        }

        public async Task<RegisterModel> GetuserModelAsync(RegisterModel model)
        {           
            
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = Mapper.Map<User>(model);
                var isSucces = await _userManager.CreateAsync(user);
                if(isSucces.Succeeded == true)
                {
                    await _signInManager.SignInAsync(user,false);
                }                

                return model;
            }
        }

        
    }
}
