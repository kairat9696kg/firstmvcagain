﻿using AutoMapper;
using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.UnitOfWorks;
using FirstMVCAgain.UI.Models;
using FirstMVCAgain.UI.Services;
using FirstMVCAgain.UI.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FirstMVCAgain.UI.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ProductService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateProduct(ProductCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {

                var product = Mapper.Map<Product>(model);

                using (var binaryReader = new BinaryReader(model.Image.OpenReadStream()))
                {
                     product.Image = binaryReader.ReadBytes((int)model.Image.Length);
                }

                unitOfWork.Products.Create(product);
            }
        }

        public ProductModel DetailsProduct(int id)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetByIdAndIncludAll(id);
                return Mapper.Map<ProductModel>(product);
            }
        }

        public SelectList GetBrandSelectList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();

                return new SelectList(brands, nameof(Brand.Id), nameof(Brand.Name));
            }
        }

        public SelectList GetCategorySelectList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();

                return new SelectList(categories, nameof(Category.Id), nameof(Category.Name));
            }
        }

        public ProductEditModel GetProductById(int id)
        {

            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetByIdWithCategoryAndBrand(id);
                var editModel = Mapper.Map<ProductEditModel>(product);

                var categories = unitOfWork.Categories.GetAll();
                var brands = unitOfWork.Brands.GetAll();

                editModel.CategoryList = new SelectList(categories,
                    nameof(Category.Id),
                    nameof(Category.Name),
                    editModel.CategoryId);

                editModel.BrandList = new SelectList(brands,
                    nameof(Brand.Id),
                    nameof(Brand.Name),
                    editModel.BrandId);

                return editModel;
            }
        }

        public ProductCreateModel GetProductCreateModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                var brands = unitOfWork.Brands.GetAll().ToList();

                return new ProductCreateModel()
                {
                    CategoryList = new SelectList(categories, nameof(Category.Id), nameof(Category.Name)),
                    BrandList = new SelectList(brands, nameof(Brand.Id), nameof(Brand.Name))
                };
            }
        }

        public void RemoveProduct(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetById(id);
                unitOfWork.Products.Remove(product);
            }
        }

        public List<ProductModel> SearchProduct(ProductFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var from = model.PriceFrom == null ? decimal.MinValue : model.PriceFrom.Value;
                var To = model.PriceTo == null ? decimal.MaxValue : model.PriceTo.Value;

                var products = unitOfWork.Products.GetAllWithCategoryAndBrand();

                products = products.ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByName(model.Name)
                    .ByCategories(model.CategoryId)
                    .ByBrands(model.BrandId);



                List<ProductModel> productModels = Mapper.Map<List<ProductModel>>(products);
                productModels = productModels.SortProducts(model.SortType, model.SortDirection).ToList();

                int pageSize = 3;
                int count = productModels.Count;
                int Page = model.Page.HasValue ? model.Page.Value : 1;

                productModels = productModels.Skip((Page - 1) * pageSize).Take(pageSize).ToList();
                PagingModel pagingModel = new PagingModel(count, Page, pageSize);
                model.PagingModel = pagingModel;
                model.Page = Page;

                return productModels;
            }
        }


        public void UpdateProduct(ProductEditModel editModel)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = Mapper.Map<Product>(editModel);
                unitOfWork.Products.Update(product);
            }

        }

        public void UploadImages(AddGalleryImageModel model)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                foreach( var image in model.Images)
                {
                    var galleryImage = new GalleryImage()
                    {
                        ProductId = model.Productid,
                        Name = image.FileName,
                        Image = GetImageBytes(image)
                    };
                    unitOfWork.GalleryImage.Create(galleryImage);
                }
            }
        }
        private byte[] GetImageBytes(IFormFile file)
        {
            using(var binaryReader = new BinaryReader(file.OpenReadStream()))
            {
                return binaryReader.ReadBytes((int)file.Length);
            }
        }
    }
}
