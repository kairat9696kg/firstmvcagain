﻿using AutoMapper;
using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.UnitOfWorks;
using FirstMVCAgain.UI.Models;
using FirstMVCAgain.UI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstMVCAgain.UI.Services
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateBrand(BrandModel brandModel)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll();
                var count = 0;
                foreach(var model in brands)
                {
                    if(model.Name == brandModel.Name)
                    {
                        count++;

                    }
                }

                if(count == 0)
                {
                    Brand brand = Mapper.Map<Brand>(brandModel);
                    unitOfWork.Brands.Create(brand);
                }
            }
        }

        public IEnumerable<BrandModel> GetAllBrandModels()
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();

                List<BrandModel> brandModels = Mapper.Map<List<BrandModel>>(brands);
                return brandModels;
            }
        }
    }
}
