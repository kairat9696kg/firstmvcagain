﻿using FirstMVCAgain.UI.Models;
using System.Collections.Generic;

namespace FirstMVCAgain.UI.Services.Contracts
{
    public interface ICategoryService
    {
        IEnumerable<CategoryModel> GetAllCategiryModels();
        void CreateCategory(CategoryModel model);
    }
}
