﻿using FirstMVCAgain.UI.Models;
using System.Collections.Generic;

namespace FirstMVCAgain.UI.Services.Contracts
{
    public interface IBrandService
    {
        IEnumerable<BrandModel> GetAllBrandModels();
        void CreateBrand(BrandModel model);
    }
}
