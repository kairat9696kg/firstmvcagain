﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.UI.Services.Contracts
{
    public interface IAccountService
    {
        Task<RegisterModel> GetuserModelAsync(RegisterModel model);
        User GetUser(string email);
    }
}
