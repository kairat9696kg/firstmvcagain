﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.UI.Services
{
    public static class ProductFilterExtensions
    {
        public static IEnumerable<Product> ByPriceFrom(this IEnumerable<Product> products, decimal? priceFrom)
        {
            if (priceFrom.HasValue)
                return products.Where(p => p.Price >= priceFrom.Value);
            return products;
        }

        public static IEnumerable<Product> ByPriceTo(this IEnumerable<Product> products, decimal? priceTo)
        {
            if (priceTo.HasValue)
                return products.Where(p => p.Price <= priceTo.Value);
            return products;
        }

        public static IEnumerable<Product> ByName(this IEnumerable<Product> products, string Name)
        {
            if (!string.IsNullOrWhiteSpace(Name))
                return products.Where(p => p.Name.Contains(Name));
            return products;
        }

        public static IEnumerable<Product> ByCategories(this IEnumerable<Product> products, int? category)
        {
            if (category != null)
                return products.Where(p => p.CategoryId == category);
            return products;
        }


        public static IEnumerable<Product> ByBrands(this IEnumerable<Product> products, int? brand)
        {
            if (brand != null)
                return products.Where(p => p.BrandId == brand);
            return products;
        }

        public static IEnumerable<ProductModel> SortProducts(this IEnumerable<ProductModel> products,
            SortType sortType, SortDirection sortDirection)
        {
            switch (sortType)
            {
                case SortType.Name:
                    products = products.GetSort(sortDirection, p => p.Name);
                    break;

                case SortType.Brand:
                    products = products.GetSort(sortDirection, p => p.BrandName);
                    break;
                case SortType.Category:
                    products = products.GetSort(sortDirection, p => p.CategoryName);
                    break;
                case SortType.Price:
                    products = products.GetSort(sortDirection, p => p.Price);
                    break;
            }
            return products;
        }

        public static IEnumerable<ProductModel> GetSort<T>(this IEnumerable<ProductModel> products,
            SortDirection sortDirection,
            Func<ProductModel, T> keySelector)
        {
            return sortDirection == SortDirection.Asc ?
                        products.OrderBy(keySelector) :
                        products.OrderByDescending(keySelector);
        }
    }
}
