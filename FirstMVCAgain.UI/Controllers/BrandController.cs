﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCAgain.UI.Models;
using FirstMVCAgain.UI.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCAgain.UI.Controllers
{
    public class BrandController : Controller
    {
        private IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            if (brandService == null)
                throw new ArgumentNullException(nameof(brandService));
            _brandService = brandService;
        }

        public IActionResult Index()
        {
            try
            {
                var brandModels = _brandService.GetAllBrandModels().ToList(); ;
                return View(brandModels);
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(BrandModel model)
        {
            try
            {
                _brandService.CreateBrand(model);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
