﻿using FirstMVCAgain.UI.Models;
using FirstMVCAgain.UI.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace FirstMVCAgain.UI.Controllers
{
    public class ProductController : Controller
    {
        public IProductService _productService;
        

        public ProductController(IProductService productService)
        {
            if (productService == null)
                throw new ArgumentNullException(nameof(productService));
            _productService = productService;
        }


        [HttpGet]
        public IActionResult Index(ProductFilterModel model)
        {
            try
            {
                var products = _productService.SearchProduct(model);

                model.Products = products;
                model.CategoryList = _productService.GetCategorySelectList();
                model.BrandList = _productService.GetBrandSelectList();
                return View(model);
            }
            catch(ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
                    
        }

        [HttpGet]
        public IActionResult Create()
        {
            try
            {
                var model = _productService.GetProductCreateModel();
                return View(model);
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost]
        public IActionResult Create(ProductCreateModel model)
        {

            try
            {
                _productService.CreateProduct(model);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }           
        }


        
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequastMessage = "Product id cannot be null";
                return View("BadRequest");
            }
            var product = _productService.GetProductById(id.Value);
            return View(product);
        }

        [HttpPost]
        public IActionResult Edit( ProductEditModel editModel)
        {
            _productService.UpdateProduct(editModel);
            return RedirectToAction("Index");
        }

        public IActionResult Remove(int? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequastMessage = "This product cannot be finding";
                return View("BadRequest");
            }
            _productService.RemoveProduct(id.Value);
            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult Details(int Id)
        {
            ProductModel product = _productService.DetailsProduct(Id);
            return View(product);
        }

        [HttpPost]
        public IActionResult Upload(AddGalleryImageModel model)
        {
            _productService.UploadImages(model);

            return RedirectToAction("Details", new {Id = model.Productid });
        }
    }
}
