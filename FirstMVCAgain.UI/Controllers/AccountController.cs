﻿using AutoMapper;
using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.UI.Models;
using FirstMVCAgain.UI.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.UI.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IAccountService _serviceAccount;
        private readonly SignInManager<User> _signInManager;

        public AccountController(UserManager<User> userManager, IAccountService serviceAccount,SignInManager<User> signInManager)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            if (serviceAccount == null)
                throw new ArgumentNullException(nameof(serviceAccount));
            if (signInManager == null)
                throw new ArgumentNullException(nameof(signInManager));
            _signInManager = signInManager;
            _userManager = userManager;
            _serviceAccount = serviceAccount;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegisterAsync(RegisterModel model)
        {
            User user = _serviceAccount.GetUser(model.Email);
            if (user != null)
            {
                ModelState.AddModelError("Email", "Пользователь с таким Email уже существует");
                return View(model);
            }                   
            
            await _serviceAccount.GetuserModelAsync(model);

            return RedirectToAction("My");
        }

        public async Task<IActionResult> My()
        {
            User currentUser = await _userManager.GetUserAsync(User);
            var userModel = Mapper.Map<UserModel>(currentUser);
            return View(userModel);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
           
            return RedirectToAction("Index", "Product");
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoginAsync(LoginModel loginModel)
        {
            var user =  _serviceAccount.GetUser(loginModel.Email);
            if (user != null && ModelState.IsValid)
            {
                
                if (user.Email == loginModel.Email)
                {
                    if (user.Password == loginModel.Password)
                    {                        
                        await _signInManager.SignInAsync(user, false);
                        return RedirectToAction("My");
                    }
                    ModelState.AddModelError("", "Пароль не совподает");
                    return View(loginModel);
                }
                ModelState.AddModelError("", "Логин не совподает");
                return View(loginModel);
            }
            else
            {
                ModelState.AddModelError("", "Пароль или логин не совподает");
                return View(loginModel);
            }
        }

    }
}
