﻿using FirstMVCAgain.UI.Models;
using FirstMVCAgain.UI.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace FirstMVCAgain.UI.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));
            _categoryService = categoryService;
        }

        public IActionResult Index()
        {
            try
            {
                var categories = _categoryService.GetAllCategiryModels().ToList();
                return View(categories);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CategoryModel model)
        {
            try
            {
                _categoryService.CreateCategory(model);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
