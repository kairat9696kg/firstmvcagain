﻿using FirstMVCAgain.UI.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAgain.UI.Models
{
    public class ProductFilterModel
    {
        [Display(Name = "По названию")]
        public string Name { get; set; }

        [Display(Name = "По категории")]
        public int? CategoryId { get; set; }

        [Display(Name = "По бренду")]
        public int? BrandId { get; set; }

        [Display(Name = "От")]
        public decimal? PriceFrom { get; set; }

        [Display(Name = "До")]
        public decimal? PriceTo { get; set; }
        public int? Page { get; set; }
        public SortType SortType { get; set; }
        public SortDirection SortDirection { get; set; }
        public List<ProductModel> Products{ get; set; }
        public SelectList CategoryList { get; set; }
        public SelectList BrandList { get; set; }
        public PagingModel PagingModel { get; set; }

        
    }
}

