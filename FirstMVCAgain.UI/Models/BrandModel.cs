﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCAgain.UI.Models
{
    public class BrandModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Укажите Название")]
        [Display(Name = "Название")]
        [MaxLength(100, ErrorMessage = "Максимальное колличество символов 100")]
        public string Name { get; set; }
    }
}
