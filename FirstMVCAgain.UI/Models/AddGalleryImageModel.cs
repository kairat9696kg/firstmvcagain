﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.UI.Models
{
    public class AddGalleryImageModel
    {
        [Required]
        public int Productid { get; set; }
        public IFormFileCollection Images { get; set; }
    }
}
