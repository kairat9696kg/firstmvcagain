﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.UI.Models
{
    public enum SortType
    {
        Name,
        Brand,
        Category,
        Price,
    }

    public enum SortDirection
    {
        Asc,
        Desc,
    }
}
