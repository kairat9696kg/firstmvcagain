﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace FirstMVCAgain.UI.Models
{
    public class ProductModel
    {
        public static string NoBrand = "No Brand";
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string BrandName { get; set; }
        public string CategoryName { get; set; }
        public byte[] Image { get; set; }
        public List<GalleryimageModel> GalleryImages { get; set; }
    }
}
