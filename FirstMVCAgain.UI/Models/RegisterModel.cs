﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAgain.UI.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Поле \"Имя\" должно быть заполненно")]
        [Display(Name = "Имя")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]

        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Используйте только латинские буквы буквы!")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Поле \"Фамилия\" должно быть заполненно")]
        [Display(Name = "Фамилия")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]

        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Используйте только латинские буквы буквы!")]
        public string SurName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthdayDate { get; set; }

        [Required(ErrorMessage = "Поле \"MobilePhone\" должно быть заполненно")]
        [Display(Name = "MobilePhone")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Поле \"Email\" должно быть заполненно")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле \"Пароль\" должно быть заполненно")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Поле \"Подтвердить пароль\" должно быть заполненно")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }
    }
}
