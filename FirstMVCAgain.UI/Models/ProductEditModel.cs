﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAgain.UI.Models
{
    public class ProductEditModel
    {
        public int Id{get; set;}
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Категория")]
        [Required]
        public int CategoryId { get; set; }

        [Display(Name = "Бренд")]
        public int? BrandId { get; set; }

        [Display(Name = "Цена")]
        [Range(1, double.PositiveInfinity, ErrorMessage = "Цена должна быть больше нуля")]
        [Required(ErrorMessage = "Цена должна быть указана")]
        public decimal Price { get; set; }
        public SelectList CategoryList { get; set; }
        public SelectList BrandList { get; set; }
    }
}
