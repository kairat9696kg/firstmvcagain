﻿using AutoMapper;
using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateGalleryImageToGalleryImageModel();
            CreateUserToUserModel();
            CreateRegisterModelToUserModel();
            CreateRegisterModelToUser();
            CreateProductEditModelToProduct();
            CreateProductToProductEditModel();
            CreateCategoryModelToCategory();
            CreateBrandModelToBrandMap();
            CreateProductCreateModelToProductMap();
            CreateBrandToBrandModelMap();
            CreateCategoryToCategoryModdelMap();
            CreateProductToProductModelMap();
        }


        private void CreateGalleryImageToGalleryImageModel()
        {
            CreateMap<GalleryImage,GalleryimageModel>();
        }
        private void CreateUserToUserModel()
        {
            CreateMap<User, UserModel>();
        }

        private void CreateRegisterModelToUserModel()
        {
            CreateMap<RegisterModel, UserModel>();
        }

        private void CreateRegisterModelToUser()
        {
            CreateMap<RegisterModel, User>();
        }
        private void CreateProductEditModelToProduct()
        {
            CreateMap<ProductEditModel, Product>();
        }

        private void CreateProductToProductEditModel()
        {
            CreateMap<Product, ProductEditModel>();
        }

        private void CreateCategoryModelToCategory()
        {
            CreateMap<CategoryModel, Category>();
        }

        private void CreateBrandModelToBrandMap()
        {
            CreateMap<BrandModel, Brand>();
        }

        private void CreateProductCreateModelToProductMap()
        {
            CreateMap<ProductCreateModel, Product>()
                .ForMember(p => p.Image,
                src => src.Ignore());
        }

        private void CreateBrandToBrandModelMap()
        {
            CreateMap<Brand, BrandModel>();
        }
        private void CreateCategoryToCategoryModdelMap()
        {
            CreateMap<Category, CategoryModel>();
        }


        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                src => src.MapFrom(p => p.Category.Name))
                .ForMember(target => target.GalleryImages,
                src => src.MapFrom(p => p.GalleryImages));            
        }

    }
}

