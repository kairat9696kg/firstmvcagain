﻿using FirstMVCAgain.Models;
using System.Collections.Generic;

namespace FirstMVCAgain.Services.Contracts
{
    public interface IBrandService
    {
        IEnumerable<BrandModel> GetAllBrandModels();
        void CreateBrand(BrandModel model);
    }
}
