﻿using FirstMVCAgain.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.Services.Contracts
{
    public interface IProductService
    {
        List<ProductModel> SearchProduct(ProductFilterModel model);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
        SelectList GetCategorySelectList();
        SelectList GetBrandSelectList();
        ProductEditModel GetProductById(int id);
        void UpdateProduct(ProductEditModel editModel);
        void RemoveProduct(int id);
    }
}
