﻿using FirstMVCAgain.Models;
using System.Collections.Generic;

namespace FirstMVCAgain.Services.Contracts
{
    public interface ICategoryService
    {
        IEnumerable<CategoryModel> GetAllCategiryModels();
        void CreateCategory(CategoryModel model);
    }
}
