﻿using AutoMapper;
using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.UnitOfWorks;
using FirstMVCAgain.Models;
using FirstMVCAgain.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstMVCAgain.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateCategory(CategoryModel model)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll();
                var count = 0;
                foreach(var cat in categories)
                {
                    if (cat.Name == model.Name)
                        count++;
                }

                if(count == 0)
                {
                    Category category = Mapper.Map<Category>(model);
                    unitOfWork.Categories.Create(category);
                }
            }
        }

        public IEnumerable<CategoryModel> GetAllCategiryModels()
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                return Mapper.Map<List<CategoryModel>>(categories);
            }
        }
    }
}
