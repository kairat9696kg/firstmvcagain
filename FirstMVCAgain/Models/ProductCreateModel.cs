﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAgain.Models
{
    public class ProductCreateModel
    {
        [Display(Name = "Название")]
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        public string Name { get; set; }

        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Категория должна быть указана")]
        public int CategoryId { get; set; }

        [Display(Name = "Бренд")]
        public int? BrandId { get; set; }

        [Display(Name = "Цена")]
        [Range(1, double.PositiveInfinity )]
        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "Цена должна быть указана")]
        public decimal Price { get; set; }
        public SelectList CategoryList { get; set; }
        public SelectList BrandList { get; set; }
    }
}
