﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Укажите Название")]
        [Display(Name = "Название")]
        [MaxLength(100, ErrorMessage = "Максимальное колличество символов 100")]
        public string Name { get; set; }
    }
}
