﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCAgain.Models
{
    public class PagingModel
    {
        public int PageNumber { get; set; }
        public int TotalPAge { get; set; }

        public PagingModel(int count ,int pageNumber, int pageSize )
        {
            PageNumber = pageNumber;
            TotalPAge = (int)Math.Ceiling(count /(double)pageSize);
        }

        public bool HasPreviousPage => PageNumber > 1;
        public bool HasNextPage => PageNumber < TotalPAge;
    }
}
