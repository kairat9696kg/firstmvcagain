﻿namespace FirstMVCAgain.Models
{
    public class ProductModel
    {
        public static string NoBrand = "No Brand";
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string BrandName { get; set; }
        public string CategoryName { get; set; }
    }
}
