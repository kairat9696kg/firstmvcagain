﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCAgain.DAL.Entities
{
    public class GalleryImage : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
