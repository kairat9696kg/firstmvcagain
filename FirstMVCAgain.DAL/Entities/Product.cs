﻿using System.Collections.Generic;

namespace FirstMVCAgain.DAL.Entities
{
    public class Product : IEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }

        public int? BrandId { get; set; }
        public Brand Brand { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int Id { get; set; }
        public byte[]? Image { get; set; }
        public ICollection<GalleryImage> GalleryImages { get; set; }
    }
}
