﻿using System.Collections.Generic;

namespace FirstMVCAgain.DAL.Entities
{
    public class Category : IEntity
    {
        public string Name { get; set; }

        public ICollection<Product> Products { get; set; }
        public int Id { get; set ; }
    }
}
