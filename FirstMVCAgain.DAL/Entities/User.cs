﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FirstMVCAgain.DAL.Entities
{
    public class User : IdentityUser<int>,IEntity
    {      
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Password { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime BirthdayDate { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
