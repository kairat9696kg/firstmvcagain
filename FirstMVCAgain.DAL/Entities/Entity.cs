﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCAgain.DAL.Entities
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
