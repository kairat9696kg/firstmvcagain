﻿using FirstMVCAgain.DAL.EntityConfigurationContainer;
using Microsoft.EntityFrameworkCore;
using System;

namespace FirstMVCAgain.DAL
{
    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions _options;
        private readonly IEntityConfigurationContainer _entityConfigurationContainer;

        public ApplicationDbContextFactory(
            DbContextOptions options,
            IEntityConfigurationContainer entityConfigurationContainer)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));
            if (entityConfigurationContainer == null)
                throw new ArgumentNullException(nameof(entityConfigurationContainer));

            _entityConfigurationContainer = entityConfigurationContainer;
            _options = options;
        }

        public ApplicationDbContext Create()
        {
            return new ApplicationDbContext(_options, _entityConfigurationContainer);
        }
    }
}
