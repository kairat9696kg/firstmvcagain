﻿using FirstMVCAgain.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCAgain.DAL.EntityConfiguration
{
    public class BrandConfiguration : BaseEntityConfiguration<Brand>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Brand> builder)
        {
            builder.HasMany(b => b.Products)
                 .WithOne(b => b.Brand)
                 .HasForeignKey(b => b.BrandId);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Brand> builder)
        {
            builder.Property(b => b.Name)
               .HasMaxLength(200)
               .IsRequired();

            builder.HasIndex(b => b.Name)
                .IsUnique(false);
        }
    }
}
