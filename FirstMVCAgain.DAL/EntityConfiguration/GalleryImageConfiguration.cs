﻿using FirstMVCAgain.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCAgain.DAL.EntityConfiguration
{
    public class GalleryImageConfiguration : BaseEntityConfiguration<GalleryImage>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<GalleryImage> builder)
        {
            builder.HasOne(p => p.Product)
                .WithMany(p => p.GalleryImages)
                .HasForeignKey(p => p.ProductId)
                .IsRequired();
        }
    }
}
