﻿using FirstMVCAgain.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCAgain.DAL.EntityConfiguration
{
    public class CategoryConfiguration : BaseEntityConfiguration<Category>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Category> builder)
        {
            builder.HasMany(c => c.Products)
                .WithOne(c => c.Category)
                .HasForeignKey(c => c.CategoryId)
                .IsRequired();
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Category> builder)
        {            
            builder.Property(c => c.Name)
                .HasMaxLength(200)
                .IsRequired();

            builder.HasIndex(c => c.Name)
                .IsUnique(false);
        }
    }
}
