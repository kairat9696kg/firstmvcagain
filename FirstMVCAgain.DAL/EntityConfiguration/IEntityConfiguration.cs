﻿using FirstMVCAgain.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace FirstMVCAgain.DAL.EntityConfiguration
{
    public interface IEntityConfiguration<T> where T : class,IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
