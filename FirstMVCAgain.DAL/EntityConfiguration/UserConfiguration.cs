﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.EntityConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EnterprisePortal.DAL.EntitiesConfiguration
{
    public class UserConfiguration : BaseEntityConfiguration<User>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<User> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.SurName)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Email)
                .HasMaxLength(100)
                .IsRequired();

            
            builder
              .Property(b => b.PhoneNumber)
              .HasMaxLength(100)
              .IsRequired();

            builder
              .Property(b => b.BirthdayDate)
              .IsRequired();            
        }

        
    }
}
