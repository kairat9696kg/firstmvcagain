﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FirstMVCAgain.DAL.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Products;
        }

        public IEnumerable<Product> GetAllWithCategoryAndBrand()
        {
            return entities
                .Include(e => e.Brand)
                .Include(e => e.Category)
                .ToList();
        }

        public IEnumerable<Product> GetAllWithCategoryAndBrandByPrice(decimal priceFrom, decimal priceTo)
        {
            return GetAllWithCategoryAndBrand()
                .Where(e => e.Price >= priceFrom && e.Price <= priceTo)
                .ToList();                
        }

       
        public Product GetByIdWithCategoryAndBrand(int id)
        {
            return entities
                .Include(p => p.Category)
                .Include(p => p.Brand)
                .FirstOrDefault(e => e.Id == id);
        }
        public Product GetByIdAndIncludAll(int id)
        {
            return entities
               .Include(e => e.Brand)
               .Include(e => e.Category)
               .Include(e => e.GalleryImages)
               .FirstOrDefault(e => e.Id == id);
        }

        public Product GetTheMostExpensiveProduct()
        {
            return entities.OrderByDescending(e => e.Price).First();
        }
    }
}
