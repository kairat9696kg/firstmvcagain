﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.Repositories.Contracts;

namespace FirstMVCAgain.DAL.Repositories
{
    public class BrandRepository : Repository<Brand>,IBrandRepository 
    {
        public BrandRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Brands;
        }
    }
}
