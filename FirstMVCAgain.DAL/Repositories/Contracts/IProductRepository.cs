﻿using FirstMVCAgain.DAL.Entities;
using System.Collections.Generic;

namespace FirstMVCAgain.DAL.Repositories.Contracts
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetTheMostExpensiveProduct();
        IEnumerable<Product> GetAllWithCategoryAndBrand();
        IEnumerable<Product> GetAllWithCategoryAndBrandByPrice(decimal priceFrom, decimal priceTo);
        Product GetByIdWithCategoryAndBrand(int id);
        Product GetByIdAndIncludAll(int id);
    }
}
