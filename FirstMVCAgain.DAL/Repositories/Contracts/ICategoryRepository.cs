﻿using FirstMVCAgain.DAL.Entities;

namespace FirstMVCAgain.DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
        
    }
}
