﻿using FirstMVCAgain.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCAgain.DAL.Repositories.Contracts
{
    public interface IGalleryImageRepository : IRepository<GalleryImage>
     {
    }
}
