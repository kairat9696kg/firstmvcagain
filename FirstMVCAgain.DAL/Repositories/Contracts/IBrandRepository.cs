﻿using FirstMVCAgain.DAL.Entities;

namespace FirstMVCAgain.DAL.Repositories.Contracts
{
    public interface IBrandRepository : IRepository<Brand> 
    {
    }
}
