﻿using FirstMVCAgain.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCAgain.DAL.Repositories.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserList(string Email);
    }
}
