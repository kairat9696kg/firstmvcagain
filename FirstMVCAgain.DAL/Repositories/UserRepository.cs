﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.Repositories.Contracts;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstMVCAgain.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Users;
        }

        public User GetUserList(string email)
        {
            return entities.FirstOrDefault(u => u.Email == email);
        }
    }
}
