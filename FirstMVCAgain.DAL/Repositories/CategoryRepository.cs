﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.Repositories.Contracts;

namespace FirstMVCAgain.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>,ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context): base(context)
        {
            entities = context.Categories;
        }
    }
}
