﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.EntityConfiguration;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCAgain.DAL.EntityConfigurationContainer
{
    public interface IEntityConfigurationContainer
    {
        IEntityConfiguration<Product> ProductConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Brand> BrandConfiguration { get; }
        IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; }
    }
}
