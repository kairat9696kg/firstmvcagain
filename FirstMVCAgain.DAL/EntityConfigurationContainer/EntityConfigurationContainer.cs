﻿using FirstMVCAgain.DAL.Entities;
using FirstMVCAgain.DAL.EntityConfiguration;

namespace FirstMVCAgain.DAL.EntityConfigurationContainer
{
    public class EntityConfigurationContainer : IEntityConfigurationContainer
    {
        public IEntityConfiguration<Product> ProductConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<Brand> BrandConfiguration { get; }
        public IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; set; }

        public EntityConfigurationContainer()
        {
            ProductConfiguration = new ProductConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            BrandConfiguration = new BrandConfiguration();
            GalleryImageConfiguration = new GalleryImageConfiguration();
        }
    }
}
